#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
TimeLapse.py (bitbucket)

Xavi Monllau <xavi@monllau.es> 06/2014

dependencies de executables:
avconv, imagemagick.
'''

import os
import sys
import xmon
import time
import shutil
from configparser import SafeConfigParser
from argparse import ArgumentParser


def main():

	#Valores por defecto desde un fichero
	app_cfg = os.getenv("HOME")+"/.xmon"
	cfg = app_cfg+"/TimeLapse.cfg"
	if not os.path.exists(cfg):
		print ('Me falta el fichero de configuracion: '+cfg)
		sys.exit(0)
	else:
		cparser = SafeConfigParser()
		cparser.read(cfg)

	#Argumentos de la linea de comandos
	argp = ArgumentParser(description = 'Creacion de videos TimeLapses y StopMotion', epilog = 'Copyright 2014 xavi@monllau.es bajo licencia GPL v3.0')
	argp.add_argument('-v','--version', action='version', version='%(prog)s 1.0')
	argp.add_argument('--vdef', default = cparser.getboolean('PorDefecto','vdef'), type = bool, help = 'Visualiza parametros por defecto.')
	argp.add_argument('-i', '--idi', default = cparser.get('PorDefecto','idi'), type = str, help = 'Directorio de entrada.')
	argp.add_argument('--idii', default = cparser.get('PorDefecto','idii'), type = int, help = 'imagenen inicial a procesar.')
	argp.add_argument('--idif', default = cparser.get('PorDefecto','idif'), type = int, help = 'imagenen final a procesar.')
	argp.add_argument('--range', default = cparser.get('PorDefecto','range'), type = int, help = 'rango de imagenenes.')
	argp.add_argument('-o', '--odi', default = cparser.get('PorDefecto','odi'), type = str, help = 'Directorio de salida.')
	argp.add_argument('--pofi', default = cparser.get('PorDefecto','pofi'), type = str, help = 'Pre-Fichero de salida.')
	argp.add_argument('--ofi', default = cparser.get('PorDefecto','ofi'), type = str, help = 'Fichero de salida.')
	argp.add_argument('-f', '--fps', default = cparser.get('PorDefecto','fps'), type = str, help = 'Numero de imagenes por segundo.')
	argp.add_argument('--hd', default = cparser.get('PorDefecto','hd'), type = str, choices=['sameq', 'cga', 'vga', 'hd480', 'tdt', 'hd720', 'hd768', 'hd1080', 'hd2k', 'hd4k', 'hd8k'], help = 'Tamaño del video')
	argp.add_argument('-a', '--aspect', default = cparser.get('PorDefecto','aspect'), type = str, help = 'Aspecto del video.')
	argp.add_argument('--vext', default = cparser.get('PorDefecto','vext'), type = str, help = 'Extension del video', choices=['mp4', 'mpg', 'mpeg', 'flv', 'mov', 'ogg', 'mkv', 'avi'])
	argp.add_argument('--flink', default = cparser.get('PorDefecto','flink'), type = str, help = 'Formato de los links.')
	argp.add_argument('-c', '--crop', default = cparser.get('PorDefecto','crop'), type = str, help = 'Recortamos la imagen.')
	argp.add_argument('--play', default = cparser.getboolean('PorDefecto','play'), type = bool, help = 'Visualizamos el video?')
	argp.add_argument('-m', '--mon', default = cparser.getboolean('PorDefecto','mon'), type = bool, help = 'Monitorizamos?')
	argp.add_argument('-s', '--stad', default = cparser.getboolean('PorDefecto','stad'), type = bool, help = 'Visualizamos estadisticas?')
	argp.add_argument('-b', '--btmp', default = cparser.getboolean('PorDefecto','btmp'), type = bool, help = 'Borramos temporales?')
	argp.add_argument('--filtro', default = cparser.get('PorDefecto','filtro'), type = str, help = 'Añadimos filtros.')
	argp.add_argument('--mosca', default = cparser.get('PorDefecto','mosca'), type = str, help = 'Añadimos una mosca.')
	argp.add_argument('--pointsize', default = cparser.get('PorDefecto','pointsize'), type = str, help = 'Tamaño de la letra de la mosca.')
	argp.add_argument('--resize', default = cparser.get('PorDefecto','resize'), type = str, help = 'modifica tamaño del png.')
	argp.add_argument('-n', '--normalize', default = cparser.getboolean('PorDefecto','normalize'), type = bool, help = 'normaliza el rango de la imagen.')
	argp.add_argument('--quality', default = cparser.get('PorDefecto','quality'), type = str, help = 'Calidad de la imagen.')
	argp.add_argument('--charcoal', default = cparser.get('PorDefecto','charcoal'), type = str, help = 'Añade efecto carboncillo.')
	argp.add_argument('-p', '--paint', default = cparser.get('PorDefecto','paint'),type = str, help = 'Añade efecto oleo.')
	argp.add_argument('--sketch', default = cparser.get('PorDefecto','sketch'), type = str, help = 'Añade efecto esbozo a lapiz.')
	argp.add_argument('--unsharp', default = cparser.get('PorDefecto','unsharp'), type = str, help = 'Añade nitidez.')
	argp.add_argument('--contrast', default = cparser.get('PorDefecto','contrast'), type = str, help = 'Contraste de la imagen.')
	argp.add_argument('-r', '--raw', default = cparser.getboolean('PorDefecto','raw'), type = bool, help = 'procesamos el RAW?')
	argp.add_argument('--rawxmp', default = cparser.get('PorDefecto','rawxmp'), type = str, help = 'Fichero xmp del revelado RAW?')
	argp.add_argument('--braw', default = cparser.getboolean('PorDefecto','braw'), type = bool, help = 'Borramos los RAW?')
	argp.add_argument('--mod', default = cparser.getboolean('PorDefecto','mod'), type = bool, help = 'modificamos los fotogramas?')
	argp.add_argument('-d', '--deflicker', default = cparser.getboolean('PorDefecto','deflicker'), type = bool, help = 'corregimos el deflicker?')
	argumento = argp.parse_args()

	#Visualizacion de los parametros por defecto
	if argumento.vdef:
		for seccion in cparser.sections():
			print ('['+seccion+']')
			for opcion in cparser.items(seccion):
				print(opcion[0]+' = '+opcion[1])
		sys.exit(0)

	#Tiempo de inicio del programa
	if argumento.mon:
		inicio = time.time()
		mrun = True
	else:
		mrun = False

	#Comprovacion y correccion de parametros
	#.....comprovar que idi termine en /
	if not argumento.idi.endswith("/"):
		argumento.idi = argumento.idi+"/"
	#.....comprovar que odi termine en /
	if not argumento.odi.endswith("/"):
		argumento.odi = argumento.odi+"/"
	#.....Calidad y aspecto del video
	if argumento.hd == "hd480" or argumento.hd == "hd720" or argumento.hd == "hd1080":
		format = "-s "+argumento.hd+" -aspect "+argumento.aspect
	elif argumento.hd == "cga":
		format = "-s 320x180 -aspect "+argumento.aspect
	elif argumento.hd == "vga":
		format = "-if not argumento.nm:s 640x360 -aspect "+argumento.aspect
	elif argumento.hd == "tdt":
		format = "-s 1024x576 -aspect "+argumento.aspect
	elif argumento.hd == "hd768":
		format = "-s 1366x768 -aspect "+argumento.aspect
	elif argumento.hd == "hd2k":
		format = "-s 2048x1152 -aspect "+argumento.aspect
	elif argumento.hd == "hd4k":
		format = "-s 3840x2160 -aspect "+argumento.aspect
	elif argumento.hd == "hd5k":
		format = "-s 5120x2880 -aspect "+argumento.aspect
	elif argumento.hd == "hd8k":
		format = "-s 7680x4320 -aspect "+argumento.aspect
	else:
		format = "-aspect "+argumento.aspect

	if argumento.mod:
		#Lista de ficheros a procesar
		#imagenes = sorted(xmon.FicherosEnDir(argumento.idi))
		imagenes=xmon.BuscarFicheros(argumento.idi, extensions=['.jpg','.JPG','.raw','.RAW','.rw2','.RW2'],rec=False)

		#Procesar ficheros raw
		if argumento.raw:
			print ("procesando raw")
			odi_xmp=argumento.idi+"raw.xmp"
			home_xmp=app_cfg+"/raw/"+argumento.rawxmp+".xmp"
			if os.path.isfile(odi_xmp):
				xmp=odi_xmp+" "
			else:
				xmp=home_xmp+" "
			odi_raw = xmon.temp("raw")
			if not os.path.exists(odi_raw):
				os.makedirs(odi_raw)
			xmon.RevelarRaw(imagenes,odi_raw,xmp,'jpg',mrun)
			#imagenes=sorted(xmon.FicherosEnDir(odi_raw))
			imagenes=xmon.BuscarFicheros(odi_raw, extensions=['.jpg'],rec=False)

		#Añadir filtros varios.
		#.....calidad del fotograma
		if argumento.quality != "":
			quality = "-quality "+argumento.quality.strip()+" "
		else:
			quality = ""
		#.....añadir mosca
		if argumento.mosca != "":
			mosca = "-pointsize "+argumento.pointsize+" -draw "+argumento.mosca.strip()+" "
		else:
			mosca = ""
		#.....normaliza el rango
		if argumento.normalize:
			normalize = "-normalize "
		else:
			normalize = ""
		#.....efecto charcoal (efecto carboncillo)
		if argumento.charcoal != "":
			charcoal = "-charcoal "+argumento.charcoal.strip()+" "
		else:
			charcoal = ""
		#.....efecto paint (efecto oleo)
		if argumento.paint != "":
			paint = "-paint "+argumento.paint.strip()+" "
		else:
			paint = ""
		#.....efecto sketch (esbozo a lapiz)
		if argumento.sketch != "":
			sketch = "-sketch "+argumento.sketch.strip()+" "
		else:
			sketch = ""
		#.....efecto unsharp (nitidez)
		if argumento.unsharp != "" and argumento.unsharp != "0":
			unsharp = "-unsharp "+argumento.unsharp.strip()+" "
		else:
			unsharp = ""
		#.....efecto contrast (contraste)
		if argumento.contrast != "":
			contrast = "-contrast "+argumento.contrast.strip()+" "
		else:
			contrast = ""
		#.....Añadimos el filtro personalizado
		if argumento.filtro != "":
			filtro = argumento.filtro.strip()+" "
		else:
			filtro = ""

		#Modificaciones
		#.....Cortamos la imagen (crop)
		if argumento.crop != "":
			crop = argumento.crop.split(".")
			if len(crop) == 8:
				ancho = (int(crop[0])-int(crop[4]))/len(imagenes)
				alto = (int(crop[1])-int(crop[5]))/len(imagenes)
				xx = (int(crop[6])-int(crop[2]))/len(imagenes)
				yy = (int(crop[7])-int(crop[3]))/len(imagenes)
				anchox = int(crop[0])
				altoy = int(crop[1])
				desX = int(crop[2])
				desY = int(crop[3])
			elif len(crop) == 4:
				ccrop = "-crop "+crop[0]+"x"+crop[1]+"+"+crop[2]+"+"+crop[3]+" "
			elif len(crop) == 1:
				ccrop = "-crop "+cparser.get('crop',argumento.crop)+" "
		else:
				ccrop = ""
				crop = ""
		#.....Tamaño del jpg temporal (resize)
		if argumento.resize == '0':
			resize = " "
		elif argumento.resize == '1':
			resize = " -resize 1920x "
			if len(crop) == 8:
				if int(crop[0]) < int(crop[4]):
					resize = " -resize "+crop[0]+"x "
				else:
					resize = " -resize "+crop[4]+"x "
			if len(crop) == 4:
					resize = " -resize "+crop[0]+"x "
		elif argumento.resize == 'hd':
			pass
		else:
			resize = " -resize "+argumento.resize+"x "
		#.....Primer y ultimo fichero a procesar.
		if argumento.idii != 0:
			idini = argumento.idii
			if len(crop) == 8:
				desX = xx*argumento.idii
				desY = yy*argumento.idii
		else:
			idini = 0
		if argumento.idif != 0:
			idinf = argumento.idif
			if len(crop) == 8:
				desX = xx*argumento.idif
				desY = yy*argumento.idif
		else:
			idinf = len(imagenes)

		#Creamos el jpg final
		tmp1=xmon.temp("tl_tmp1_mod")
		for img in range (idini,idinf,argumento.range):
			if len(crop) == 8:
				anchox = anchox-ancho
				altoy = altoy-alto
				desX = desX+xx
				desY = desY+yy
				ccrop = "-crop "+str(int(anchox))+"x"+str(int(altoy))+"+"+str(int(desX))+"+"+str(int(desY))+" "
			convert = "convert "+quality+normalize+ccrop+imagenes[img]+resize+charcoal+paint+sketch+unsharp+contrast+filtro+tmp1+xmon.FicheroBase(imagenes[img])+".jpg"
			xmon.run(convert, mrun)

			if argumento.mosca != "":
				convert_mosca = "convert "+quality+mosca+tmp1+xmon.FicheroBase(imagenes[img])+".jpg "+tmp1+xmon.FicheroBase(imagenes[img])+".jpg"
				xmon.run(convert_mosca, mrun)
	else:
		tmp1=argumento.idi

	#Creacion del video desde el directorio de los links ordenados
	tmp2 = xmon.temp("tl_tmp2_links")
	xmon.CrearLinks(tmp1, tmp2, argumento.flink)
	if not os.path.exists(argumento.odi):
		os.makedirs(argumento.odi)
	if argumento.ofi != "":
		VF = argumento.odi+argumento.ofi+"."+argumento.vext
	else:
		VF = argumento.odi+argumento.pofi+argumento.hd+"."+argumento.vext

	crear_video = "avconv -r "+argumento.fps+" -i "+tmp2+"lnk"+argumento.flink+".jpg "+format+" -y -an "+VF
	xmon.run(crear_video, mrun)

	#Borramos los temporales?
	if argumento.btmp:
		if argumento.mod:
			shutil.rmtree(tmp1, 1)
		if argumento.raw:
			if argumento.braw:
				shutil.rmtree(odi_raw, 1)
			else:
				print ('No se borra braw')
	shutil.rmtree(tmp2, 1)

	#Mostramos estadisticas?
	if argumento.stad:
		if argumento.mon:
			fin = time.time() #tiempo final
			print ("\n***** Estadisticas del TimeLapse: ")
			print("Tiempo total de ejecucion =", int(fin - inicio), "segundos") #tiempo de ejecucion
			#print("Imagenes procesadas ", int((idinf-idini)/argumento.range)) #imagenes procesadas
			print("Video final: ", VF+"\n*****") #fichero de salida

	#Visualizamos el video?
	if argumento.play:
		video_play = "avplay -fs "+VF
		xmon.run(video_play, mrun)

	return 0

if __name__ == "__main__":
    main()