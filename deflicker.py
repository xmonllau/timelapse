#! /usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image, ImageEnhance
from math import sqrt
import os
import sys
import xmon


def get_image(filename):
    return Image.open(filename).convert ('RGB')

def average_rgb(image):
    sum = (0,0,0)
    width, height = image.size
    for x in range(width):
        for y in range(height):
            r, g, b = image.getpixel( (x,y) )
            ro, go, bo = sum
            sum = (r+ro, g+go, b+bo)

    return tuple([float(x)/width/height for x in sum])

def luminance(rgb):
    r,g,b = rgb
    return sqrt( 0.241*r*r + 0.691*g*g + 0.068*b*b )

def _approx_luminance(rgb):
    r,g,b = rgb
    return (0.2126*r) + (0.7152*g) + (0.0722*b)

def calculate_frame_luminances(files):
	output = []
	for n,file in enumerate(files):
		output += [ (n,file,luminance(average_rgb(get_image(file)))) ]
		print(output[-1])
	assert len(output)==len(files)
	return output

def moving_average(input, window=10):
    prefix = [input[0]]*(window/2)
    postfix = [input[-1]]*(window - window/2)
    values = prefix + input + postfix
    return [sum(values[n:n+window])/window for n in range(len(input))]

def main():
	files = xmon.BuscarFicheros(sys.argv[1], extensions=['.jpg','.JPG'],rec=False)
	num = len(files)
	output = calculate_frame_luminances(files)
	input = [item[2] for item in output]
	averages = moving_average(input)

	for (n,file,orig), smooth in zip(output,averages):
		image = get_image(file)
		new_image = ImageEnhance.Brightness(image).enhance(smooth/orig)
		path, fname = os.path.split(file)
		new_path = '/tmp/deflickered/'
		if not os.path.exists(new_path):
			os.makedirs(new_path)
		new_file = os.path.join(new_path,fname)
		print(str(n).zfill(3),file,new_file,str(output[n][2]),str(smooth),str(smooth/orig))
		new_log = os.path.join(new_path,'deflickered.log')
		xmon.log('a',new_log,str(n).zfill(3)+' '+file+' '+new_file+' '+str(output[n][2])+' '+str(smooth)+' '+str(smooth/orig))
		new_image.save(new_file)

if __name__ == "__main__":
	xmon.clear()
	main()